# Slack Slash Command

## First thing to do
run ``composer install``

## Secrets
* Go to [api.slack.com](https://api.slack.com) get your client_id and client_secret code
* Create a file in ``slack-interface`` called `secret.php` 
and past the code with your keys in it
```
<?php
   // Define Slack application identifiers
   // Even better is to put these in environment variables so you don't risk exposing
   // them to the outer world (e.g. by committing to version control)
   define( 'SLACK_CLIENT_ID', '{CLIENT_ID}' );
   define( 'SLACK_CLIENT_SECRET', '{SLACK_SECRET}' );
```
